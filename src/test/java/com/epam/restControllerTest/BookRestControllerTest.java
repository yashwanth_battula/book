package com.epam.restControllerTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.epam.customexceptions.BookNotFoundException;
import com.epam.dto.BookDTO;
import com.epam.restcontroller.BookRest;
import com.epam.service.BookService;

public class BookRestControllerTest {

	@Mock
	BookService bookService;
	
	@InjectMocks
	BookRest bookRest;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	void testGetAllBooks() {
		List<BookDTO> booksList = new ArrayList<>();
		booksList.add(new BookDTO(1L,"ab","ab","ab",1));
		when(bookService.findAll()).thenReturn(booksList);
		ResponseEntity<List<BookDTO>> responseEntity = bookRest.findAllBooks();
		assertThat(responseEntity.getBody().equals(booksList));
	}
	
	@Test
	void testGetBookById() throws BookNotFoundException {
		BookDTO bookDto = new BookDTO(1L,"abc","abc","abc",1);
		when(bookService.findById(1L)).thenReturn(bookDto);
		ResponseEntity<BookDTO> responseEntity = bookRest.findBookById(1L);
		assertThat(responseEntity.getBody().equals(bookDto));
	}
	
	@Test
	void testAddNewBook(){
		BookDTO bookDto = new BookDTO(1L,"abc","abc","abc",1);
		when(bookService.add(bookDto)).thenReturn(bookDto);
		ResponseEntity<BookDTO> responseEntity = bookRest.addNewBook(bookDto);
		assertThat(responseEntity.getBody().equals(bookDto));
	}
	
	@Test
	void testDeleteBook() throws BookNotFoundException {
		BookDTO bookDto = new BookDTO(1L,"abc","abc","abc",1);
		when(bookService.deleteById(1L)).thenReturn(true);
		ResponseEntity responseEntity = bookRest.deleteBookById(1L);
		assertThat(responseEntity.getStatusCode().equals(HttpStatus.OK));
	}
	
	@Test
	void testUpdateBook() throws BookNotFoundException{
		BookDTO bookDto = new BookDTO(1L,"abc","abc","abc",1);
		when(bookService.update(1L, bookDto)).thenReturn(bookDto);
		ResponseEntity<BookDTO> responseEntity = bookRest.updateBook(1L, bookDto);
		assertThat(responseEntity.getBody().equals(bookDto));
	}

}
