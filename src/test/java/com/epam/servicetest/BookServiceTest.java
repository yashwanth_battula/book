package com.epam.servicetest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.epam.customexceptions.BookNotFoundException;
import com.epam.dto.BookDTO;
import com.epam.entity.Book;
import com.epam.repository.BookRepository;
import com.epam.service.BookService;

public class BookServiceTest {

	@Mock
	BookRepository bookRepository;
	
	@InjectMocks
	BookService bookService;
	
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testFindAllBooks() {
		List<Book> booksList = new ArrayList<>();
		Book book = new Book(1L,"abc","abc","abc",1);
		booksList.add(book);
		when(bookRepository.findAll()).thenReturn(booksList);
		List<BookDTO> bookDTOList = new ArrayList<>();
		bookDTOList.add(new BookDTO(book));
		assertEquals(bookDTOList.size(), bookService.findAll().size());
	}
	
	@Test
	public void testFindBookById() throws BookNotFoundException {
		Book book = new Book(1L,"abc","abc","abc",1);
		Optional<Book> bookOptional = Optional.ofNullable(book);
		when(bookRepository.findById(1L)).thenReturn(bookOptional);
		BookDTO expectedBook = new BookDTO(book);
		BookDTO actualBook = bookService.findById(1L);
		assertEquals(expectedBook.getId(), actualBook.getId());
		assertEquals(expectedBook.getAuthor(), actualBook.getAuthor());
		assertEquals(expectedBook.getGenre(), actualBook.getGenre());
		assertEquals(expectedBook.getTitle(), actualBook.getAuthor());
		assertEquals(expectedBook.getPrice(), actualBook.getPrice());
	}
	
	@Test
	public void testIfNullInFindBookById() {
		when(bookRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(BookNotFoundException.class, () -> bookService.findById(1L));
	}
	
	
//	@Test
//	public void testAddNewBook() {
//		BookDTO expectedBook = new BookDTO(1L,"abc","abc","abc",1);
//		BookDTO actualBook = bookService.add(expectedBook);
//		assertEquals(expectedBook.getAuthor(), actualBook.getAuthor());
//		assertEquals(expectedBook.getGenre(), actualBook.getGenre());
//		assertEquals(expectedBook.getTitle(), actualBook.getAuthor());
//		assertEquals(expectedBook.getPrice(), actualBook.getPrice());
//	}
//	
	
	@Test
	public void testDeleteBookById() throws BookNotFoundException {
		Book book = new Book(1L,"abc","abc","abc",1);
		Optional<Book> bookOptional = Optional.ofNullable(book);
		when(bookRepository.findById(1L)).thenReturn(bookOptional);
		assertEquals(true , bookService.deleteById(1L));
	}
	
	@Test
	public void testIfNullInDeleteBook() {
		when(bookRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(BookNotFoundException.class, () -> bookService.deleteById(1L));
	}
	
//	@Test
//	public void testUpdateBook() throws BookNotFoundException {
//		Book book = new Book(1L,"abc","abc","abc",1);
//		Optional<Book> bookOptional = Optional.ofNullable(book);
//		when(bookRepository.findById(1L)).thenReturn(bookOptional);
//		BookDTO expectedBook = new BookDTO(1L,"abcd","abcd","abcd",1);
//		BookDTO actualBook = bookService.update(1L, expectedBook);
//		assertEquals(expectedBook.getAuthor(), actualBook.getAuthor());
//		assertEquals(expectedBook.getGenre(), actualBook.getGenre());
//		assertEquals(expectedBook.getTitle(), actualBook.getAuthor());
//		assertEquals(expectedBook.getPrice(), actualBook.getPrice());
//	}
	
	@Test
	public void testIfNullInUpdateBook() {
		when(bookRepository.findById(1L)).thenReturn(Optional.empty());
		assertThrows(BookNotFoundException.class, () -> bookService.update(1L , new BookDTO(1L,"abcd","abcd","abcd",1)));
	}
}
