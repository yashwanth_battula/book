package com.epam.restcontroller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.customexceptions.BookNotFoundException;
import com.epam.dto.BookDTO;
import com.epam.service.BookService;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/book")
public class BookRest {

	private Logger logger = LogManager.getLogger(BookRest.class);
	
	@Autowired
	BookService bookService;
	
	@GetMapping
	public ResponseEntity<List<BookDTO>> findAllBooks(){
		logger.info("get all books");
		return ResponseEntity.ok(bookService.findAll());
	}
	
	@GetMapping("/{bookId}")
	public ResponseEntity<BookDTO> findBookById(@PathVariable(value = "bookId") Long id){
		ResponseEntity<BookDTO> response;
		try {
			logger.info("get Book by id:{}",id);
			response = new ResponseEntity<>(bookService.findById(id) , HttpStatus.OK);
		}catch(BookNotFoundException e){
			logger.error("Exception occured while getting book with id:{}",id);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}
	
	@PostMapping
	public ResponseEntity<BookDTO> addNewBook(@RequestBody BookDTO bookDTO){
		logger.info("Add new Book");
		return ResponseEntity.ok(bookService.add(bookDTO));
	}
	
	@DeleteMapping("/{bookId}")
	public ResponseEntity<?> deleteBookById(@PathVariable(value = "bookId") Long id){
		ResponseEntity<?> response;
		try {
			logger.info("delete Book by id:{}",id);
			bookService.deleteById(id);
			response = new ResponseEntity<>(HttpStatus.OK);
		}catch(BookNotFoundException e){
			logger.error("Exception occured while deleting book with id:{}",id);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}
	
	@PutMapping("/{bookId}")
	public ResponseEntity<BookDTO> updateBook(@PathVariable(value = "bookId") Long id,@RequestBody BookDTO bookDTO){
		ResponseEntity<BookDTO> response;
		try {
			logger.info("Update book with id:{}",id);
			response = new ResponseEntity<>(bookService.update(id,bookDTO) , HttpStatus.OK);
		}catch(BookNotFoundException e){
			logger.error("Exception occured while updating book with id:{}",id);
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return response;
	}
	
}
