package com.epam.dto;

import com.epam.entity.Book;

public class BookDTO {

	private Long id;
	private String title;
	private String author;
	private String genre;
	private double price;
	
	public BookDTO(Book book) {
		this.id = book.getId();
		this.title = book.getTitle();
		this.author = book.getAuthor();
		this.genre = book.getGenre();
		this.price = book.getPrice();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public BookDTO(Long id, String title, String author, String genre, double price) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.price = price;
	}
	public BookDTO() {
	}
	@Override
	public String toString() {
		return "BookDTO [id=" + id + ", title=" + title + ", author=" + author + ", genre=" + genre + ", price=" + price
				+ "]";
	}	
}
