package com.epam.customexceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class BookNotFoundException extends RuntimeException{

	public BookNotFoundException(String exceptionMsg) {
		super(exceptionMsg);
	}
}
