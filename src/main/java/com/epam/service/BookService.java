package com.epam.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.customexceptions.BookNotFoundException;
import com.epam.dto.BookDTO;
import com.epam.entity.Book;
import com.epam.repository.BookRepository;

@Service
public class BookService {
	
	private Logger logger = LogManager.getLogger(BookService.class);


	@Autowired
	BookRepository bookRepository;

	public List<BookDTO> findAll() {
		logger.info("get all books");
		 return bookRepository.findAll().stream()
		            .map(BookDTO::new)
		            .collect(Collectors.toList());
	}

	public BookDTO findById(Long id) throws BookNotFoundException {
		logger.info("get book by id:{}",id);
		return bookRepository.findById(id)
	              .map(BookDTO::new)
	              .orElseThrow(() -> new BookNotFoundException("Book not found"));
	}

	public BookDTO add(BookDTO bookDTO) {
		logger.info("Add new book");
		Book book = new Book();
		book.setAuthor(bookDTO.getAuthor());
		book.setGenre(bookDTO.getGenre());
		book.setPrice(bookDTO.getPrice());
		book.setTitle(bookDTO.getTitle());
		Book bookAdded = bookRepository.save(book);
		return new BookDTO(bookAdded);
	}

	public Boolean deleteById(Long id) throws BookNotFoundException {
		logger.info("Delete Book by id:{}",id);
		Boolean isDeleted = false;
		if(bookRepository.findById(id).isPresent()) {
			bookRepository.deleteById(id);
			isDeleted = true;
		}else {
			logger.error("Exception occured while deleting book by id:{}",id);
			throw new BookNotFoundException("Book not found with id:"+id);
		}
		return isDeleted;
	}

	public BookDTO update(Long id, BookDTO bookDTO) throws BookNotFoundException {
		logger.info("Update book by id:{}",id);
		Book newBook = new Book(id , bookDTO.getTitle() , bookDTO.getAuthor() , bookDTO.getGenre() , bookDTO.getPrice());
		bookRepository.findById(id).map(oldBook -> bookRepository.save(newBook)).orElseThrow(() -> new BookNotFoundException("book not found with id"+id));
		return new BookDTO(newBook);
	}
	
}
