FROM openjdk:8-jre

COPY target /app

WORKDIR /app

ENTRYPOINT ["java" , "-jar" , "book-0.0.1-SNAPSHOT.jar"]